jQuery.noConflict();

(function($, PLUGIN_ID) {
  'use strict';
  $(function() {
    const message = {
      'en': {
        'msg_service_name': 'Upyun Service Name',
        'msg_operator_mame': 'Upyun Operator Name',
        'msg_operator_password': 'Upyun Operator Password',
        'msg_domain_url': 'Upyun Domain URL',
        'msg_directory': 'Directory',
        'msg_directory_description': 'Single-line text field.',
        'msg_upload': 'Upload area',
        'msg_upload_description': 'Space field (must be different from the file list area field)',
        'msg_fileList': 'File List area',
        'msg_fileList_description': 'Space field (must be different from the upload area field)',
        'msg_plugin_submit': '     Save   ',
        'msg_plugin_cancel': '     Cancel   ',
        'msg_required_field': 'Please enter the required field.',
        'msg_fields_are_same': 'The values of the "Upload area" and "File List area" fields must be different.'
      },
      'zh': {
        'msg_service_name': '又拍云服务名',
        'msg_operator_mame': '又拍云操作员名',
        'msg_operator_password': '又拍云操作密码',
        'msg_domain_url': '又拍云域名地址',
        'msg_directory': '目录字段',
        'msg_directory_description': '单行文本字段',
        'msg_upload': '上传区域字段',
        'msg_upload_description': '空白栏字段（必须与文件列表区域字段不同）',
        'msg_fileList': '文件列表区域字段',
        'msg_fileList_description': '空白栏字段（必须与上传区域字段不同）',
        'msg_plugin_submit': '     保存   ',
        'msg_plugin_cancel': '     返回   ',
        'msg_required_field': '请输入必填字段。',
        'msg_fields_are_same': '“上传区域”和“文件列表区域”字段的值必须不同。'
      }
    };

    const lang = kintone.getLoginUser().language;
    const i18n = (lang in message) ? message[lang] : message['zh'];

    $('span[datatype="translate"]').each(function(i, elt) {
      $(elt).text(i18n[$(elt).attr('data-content')]);
    });

    const config = kintone.plugin.app.getConfig(PLUGIN_ID);
    if (config['serviceName']) {
      $('#serviceName').val(config['serviceName']);
    }
    if (config['operatorName']) {
      $('#operatorName').val(config['operatorName']);
    }
    if (config['operatorPassword']) {
      $('#operatorPassword').val(config['operatorPassword']);
    }
    if (config['domainUrl']) {
      $('#domainUrl').val(config['domainUrl']);
    }

    KintoneConfigHelper.getFields('SINGLE_LINE_TEXT').then(function(resp) {
      for (let i = 0; i < resp.length; i++) {
        $('#directory').append($('<OPTION>').text(resp[i]['label']).val(resp[i]['code']));
      }
      if (config['directory']) {
        $('#directory').val(config['directory']);
      }
    }).catch(function(err) {
      console.log(err);
    });

    KintoneConfigHelper.getFields('SPACER').then(function(resp) {
      for (let i = 0; i < resp.length; i++) {
        $('#upload').append($('<OPTION>').text(resp[i]['elementId']).val(resp[i]['elementId']));
        $('#fileList').append($('<OPTION>').text(resp[i]['elementId']).val(resp[i]['elementId']));
      }
      if (config['upload']) {
        $('#upload').val(config['upload']);
      }
      if (config['fileList']) {
        $('#fileList').val(config['fileList']);
      }
    }).catch(function(err) {
      console.log(err);
    });

    $('#plugin_submit').click(function() {
      const serviceName = $('#serviceName').val().trim();
      const operatorName = $('#operatorName').val().trim();
      const operatorPassword = $('#operatorPassword').val().trim();
      const domainUrl = $('#domainUrl').val().trim();
      const directory = $('#directory').val().trim();
      const upload = $('#upload').val().trim();
      const fileList = $('#fileList').val().trim();

      if (serviceName === null || operatorName === null || operatorPassword === null || domainUrl === null || directory === null ||
        upload === null || fileList === null) {
        alert(i18n.msg_required_field);
        return;
      }
      if (serviceName.length === 0 || operatorName.length === 0 || operatorPassword.length === 0 || domainUrl.length === 0 ||
        directory.length === 0 || upload.length === 0 || fileList.length === 0) {
        alert(i18n.msg_required_field);
        return;
      }
      if (upload === fileList) {
        alert(i18n.msg_fields_are_same);
        return;
      }
      const submitConfig = {};
      submitConfig['serviceName'] = serviceName;
      submitConfig['operatorName'] = operatorName;
      submitConfig['operatorPassword'] = operatorPassword;
      submitConfig['domainUrl'] = domainUrl;
      submitConfig['directory'] = directory;
      submitConfig['upload'] = upload;
      submitConfig['fileList'] = fileList;

      kintone.plugin.app.setConfig(submitConfig);
    });

    $('#plugin_cancel').click(function() {
      history.back();
    });

  });
})(jQuery, kintone.$PLUGIN_ID);