# Upyun for Kintone
在Kintone APP内把文件上传至又拍云(Upyun)上，也可以根据需求随时在Kintone APP内进行下载。

# 又拍云(Upyun)
又拍云存储（ UPYUN Storage Service，简称 USS），是面向非结构化数据的对象存储服务，具有使用简单、高稳定、高安全等特点，满足大数据、人工智能、物联网背景下的数据传输、处理、存储、分发与容灾需求。<br>
https://www.upyun.com/products/file-storage

# 插件资源
build/upyun.zip

# 安装
将插件安装到Kintone, 请参考帮助文档[在kintone中安装插件](https://help.cybozu.cn/k/zh/admin/system_customization/add_plugin/plugin.html)

# 添加
将插件添加到Kintone App, 请参考帮助文档[在应用中添加插件](https://help.cybozu.cn/k/zh/user/app_settings/plugin.html)

# 使用
### 应用的设置
添加一个单行文本框，两个空白栏作为插件所需要的表单控件。<br>
![](./image/form.png) <br>

### 插件设置
1. 在应用管理页面中点击[插件]，然后点击Upyun for Kintone插件的齿轮图标。
2. 设置如下图的各项配置。<br>
※ 设置页面可根据登录用户设置的语言自动切换成中文、或英语（日文语言时显示中文）。<br>
![](./image/setting.png) <br>

### 使用插件
1. 创建文件夹<br>
  打开刚才使用了"Upyun for Kintone"插件的APP记录添加页面，直接点击[保存]按钮（无需填写任何信息），一条又拍云存储上相关联的文件夹结构记录就生成了，并且此文件夹结构已自动同步到又拍云存储上。（可以在又拍云存储上”服务管理-->云存储-->文件管理 “页面中看到新建的文件夹结构）。<br>
 文件夹格式：[AppId]/[LoginUserName]/[createdTime].

2. 文件上传 <br>
  打开APP记录编辑页面，通过点击[Browser]按钮选择上传文件，点击[保存]按钮，文件上传至又拍云存储上。<br>
![](./image/upload.gif) <br>

3. 图片预览 <br>
  如果将图片(gif, jpeg, png)上传到又拍云存储，则可以在Kintone app 记录详细画面，点击图片预览按钮进行图片预览<br>
  ※ 预览插件参考[Link](https://fengyuanchen.github.io/jquery-viewer/) <br>
  
### 服务设置
1. 创建服务 <br>
![](./image/upyun.png) <br>

2. 域名设置 <br>
进入服务配置里的域名管理，将域名地址设置到插件的设置中. <br>
![](./image/url.png) <br>

### 最后
完成前面所有操作后，在Kintone中就有了一个类似“文件管理”的APP。可以通过分享记录的链接来共享文件信息。

# 注意事项
本插件不直接提供又拍云存储产品，请在使用本插件前自行购买又拍云存储产品，产生的费用具体解释请参照官方[Link](https://www.upyun.com/products/file-storage) <br>
本示例代码不保证其运行,我们不为本示例代码提供技术支持。

# License
MIT License

# Copyright
Copyright(c) Cybozu, Inc.